INF = 100000
route = [
    [0, 1, 4, INF, INF, INF],
    [INF, 0, INF, 5, 1, INF],
    [INF, INF, 0, INF, INF, 2],
    [INF, INF, INF, 0, INF, 1],
    [INF, INF, INF, 1, 0, 3],
    [INF, INF, INF, INF, INF, 0]
]
# start : 0, goal : 5
start = 0
goal = 5

n = len(route)
d = [INF for i in range(n)]
prev = [-1 for i in range(n)]
visited = [0 for i in range(n)]

d[0] = start
u = 0
while sum(visited) < n:
    min = INF
    for i in range(n):
        if visited[i] == 0 and min > d[i]:
            min = d[i]
            u = i
    visited[u] = 1
    for i in range(n):
        if d[i] > route[u][i] + d[u]:
            d[i] = route[u][i] + d[u]
            prev[i] = u

way = []
now = goal
while now != -1:
    way.append(now)
    now = prev[now]

print(d[goal])
way.reverse()
print(way)




