# from, to, cost
route = [
    [0, 1, 1],
    [0, 2, 4],
    [1, 4, 1],
    [1, 3, 5],
    [2, 5, 2],
    [3, 0, 1],
    [3, 5, 1],
    [4, 3, 1],
    [4, 5, 3]
]

INF = 10000000
e = len(route)
d = [INF for i in range(6)]
prev = [-1 for i in range(6)]
start = 0
goal = 5

d[start] = 0
for line in route:
    if d[line[1]] > d[line[0]] + line[2]:
        d[line[1]] = d[line[0]] + line[2]
        prev[line[1]] = line[0]

way = []
now = goal
while now != -1:
    way.append(now)
    now = prev[now]

way.reverse()
print(d[5])
print(way)